#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if (tail == 0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	Node* tmp = new Node(el); //create a new node
	tail->next = tmp; //tmp = next of tail
	tail = tmp; //tail point at tmp

	//TO DO!
}
char List::popHead()
{
	char el = head->data;
	Node* tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	Node* g = head; //create new a pointer
	while (g->next != tail) { //if the next is not point at tail
		g = g->next; //move g to next
	}
	char add = tail->data; //save tail 
	delete tail;
	tail = g;
	// TO DO!
	return add;
	// TO DO!
	return NULL;
}
bool List::search(char el)
{
	Node* o = head; //create new a pointer
	while (o != NULL) {
		if (o->data == el) {
			return true;
		}
		o = o->next; //move o to next
	}

	// TO DO! (Function to return True or False depending if a character is in the list.
	return false; //if it not data is find untill the NULL it function will return to false
}
void List::reverse()
{
	List reversedata;
	Node* d = head; //create new a pointer
	while (d != NULL) {
		reversedata.pushToHead(d->data); //that pointed d will point to reversedata
		d = d->next;
	}
	// TO DO! (Function is to reverse the order of elements in the list.

	reversedata.print();
}
void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node* tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}