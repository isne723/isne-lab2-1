#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	mylist.pushToHead('k');
	mylist.pushToHead('e');
	mylist.pushToHead('n');
	mylist.print();
	mylist.pushToTail('c');
	mylist.pushToTail('u');
	mylist.pushToTail('t');
	mylist.pushToTail('e');
	cout << "\n";
	mylist.print();
	cout << "\ntailpop : " << mylist.popTail();
	cout << endl;
	if (mylist.search('T')) {
		cout << "FOUND T";
	}
	else {
		cout << "NOT FOUND";
	}
	cout << endl << "reversedata : ";
	mylist.reverse();

	//TO DO! Write a program that tests your list library - the code should take characters, push them onto a list, 
	//- then reverse the list to see if it is a palindrome!

}